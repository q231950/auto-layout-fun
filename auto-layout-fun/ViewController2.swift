//
//  ViewController2.swift
//  auto-layout-fun
//
//  Created by Martin Kim Dung-Pham on 08.05.16.
//  Copyright © 2016 Martin Kim Dung-Pham. All rights reserved.
//

import UIKit

class ViewController2: UIViewController {
    
    let boxViewTopAndBottomMargin = CGFloat(40)
    let boxViewLeadgingAndTrailingMargin = CGFloat(5)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.magentaColor()
        
        setupBoxView()
        setupToolBarTimer()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.setToolbarHidden(true, animated: true)
    }
    
    private func setupBoxView() {
        let boxView = UIView()
        boxView.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.3)
        boxView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(boxView)
        
        let topConstraint = boxView.topAnchor.constraintEqualToAnchor(topLayoutGuide.bottomAnchor, constant: boxViewTopAndBottomMargin)
        topConstraint.active = true
        
        let leftConstraint = boxView.leadingAnchor.constraintEqualToAnchor(view.leadingAnchor, constant: boxViewLeadgingAndTrailingMargin)
        leftConstraint.active = true
        
        let bottomConstraint = boxView.bottomAnchor.constraintEqualToAnchor(bottomLayoutGuide.topAnchor, constant: -boxViewTopAndBottomMargin)
        bottomConstraint.active = true
        
        let rightConstraint = boxView.trailingAnchor.constraintEqualToAnchor(view.trailingAnchor, constant: -boxViewLeadgingAndTrailingMargin)
        rightConstraint.active = true
    }
    
    private func setupToolBarTimer() {
        NSTimer.scheduledTimerWithTimeInterval(1.5, target: self, selector: #selector(ViewController2.toolBarTimerFired(_:)), userInfo: nil, repeats: true)
    }
    
    internal func toolBarTimerFired(timer: NSTimer) {
        navigationController?.setToolbarHidden(!(navigationController?.toolbarHidden)!, animated: true)
    }
}
