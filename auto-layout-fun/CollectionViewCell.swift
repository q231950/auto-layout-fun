//
//  CollectionViewCell.swift
//  auto-layout-fun
//
//  Created by Martin Kim Dung-Pham on 08.05.16.
//  Copyright © 2016 Martin Kim Dung-Pham. All rights reserved.
//

import UIKit

class XNGCollectionViewCell: UICollectionViewCell {
    let maxHeight = CGFloat(240)
    let minWidth = CGFloat(120)
    var item: CollectionViewItem? {
        willSet(newItem) {
            label1.text = newItem?.title1
            label2.text = newItem?.title2
            setNeedsUpdateConstraints()
        }
    }
    let label1 = UILabel()
    let label2 = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.addSubview(label1)
        contentView.addSubview(label2)
        setupLabel1()
        setupLabel2()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupLabel1() {
        label1.translatesAutoresizingMaskIntoConstraints = false
        label1.numberOfLines = 0
        label1.backgroundColor = UIColor.darkGrayColor().colorWithAlphaComponent(0.2)
        label1.textColor = UIColor.blackColor()
        label1.font = UIFont.systemFontOfSize(12)
        
        NSLayoutConstraint.activateConstraints([
            label1.topAnchor.constraintEqualToAnchor(contentView.topAnchor),
            label1.bottomAnchor.constraintEqualToAnchor(contentView.bottomAnchor),
            label1.leftAnchor.constraintEqualToAnchor(contentView.leftAnchor),
            label1.heightAnchor.constraintLessThanOrEqualToConstant(maxHeight),
            label1.widthAnchor.constraintGreaterThanOrEqualToConstant(minWidth)
            ])
    }
    
    private func setupLabel2() {
        label2.translatesAutoresizingMaskIntoConstraints = false
        label2.numberOfLines = 0
        label2.backgroundColor = UIColor.darkGrayColor().colorWithAlphaComponent(0.4)
        label2.textColor = UIColor.blackColor()
        label2.font = UIFont.italicSystemFontOfSize(10)
        
        NSLayoutConstraint.activateConstraints([
            label2.topAnchor.constraintEqualToAnchor(label1.topAnchor),
            label2.bottomAnchor.constraintEqualToAnchor(label1.bottomAnchor),
            label2.leftAnchor.constraintEqualToAnchor(label1.rightAnchor),
            label2.rightAnchor.constraintEqualToAnchor(contentView.rightAnchor),
            label2.widthAnchor.constraintGreaterThanOrEqualToConstant(minWidth)
            ])
    }
    
    override func preferredLayoutAttributesFittingAttributes(layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        let attr: UICollectionViewLayoutAttributes = layoutAttributes.copy() as! UICollectionViewLayoutAttributes
        
        var newFrame = attr.frame
        self.frame = newFrame
        
        let desiredHeight: CGFloat = self.contentView.systemLayoutSizeFittingSize(UILayoutFittingExpandedSize).height
        newFrame.size.height = desiredHeight
        attr.frame = newFrame
        return attr
    }
}
