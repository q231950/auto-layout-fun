//
//  ViewController1.swift
//  auto-layout-fun
//
//  Created by Martin Kim Dung-Pham on 08.05.16.
//  Copyright © 2016 Martin Kim Dung-Pham. All rights reserved.
//

import UIKit

class ViewController1: UIViewController {
    
    let boxViewTopAndBottomMargin = CGFloat(40)
    let boxViewLeadgingAndTrailingMargin = CGFloat(5)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.orangeColor()
        
        setupBoxView()
    }
    
    private func setupBoxView() {
        let boxView = UIView()
        boxView.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.3)
        boxView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(boxView)
        
        let topConstraint = boxView.topAnchor.constraintEqualToAnchor(topLayoutGuide.bottomAnchor, constant: boxViewTopAndBottomMargin)
        topConstraint.active = true
        
        let leftConstraint = boxView.leadingAnchor.constraintEqualToAnchor(view.leadingAnchor, constant: boxViewLeadgingAndTrailingMargin)
        leftConstraint.active = true
        
        let bottomConstraint = boxView.bottomAnchor.constraintEqualToAnchor(bottomLayoutGuide.topAnchor, constant: -boxViewTopAndBottomMargin)
        bottomConstraint.active = true
        
        let rightConstraint = boxView.trailingAnchor.constraintEqualToAnchor(view.trailingAnchor, constant: -boxViewLeadgingAndTrailingMargin)
        rightConstraint.active = true
    }
}
