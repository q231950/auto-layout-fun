//
//  ViewController3.swift
//  auto-layout-fun
//
//  Created by Martin Kim Dung-Pham on 08.05.16.
//  Copyright © 2016 Martin Kim Dung-Pham. All rights reserved.
//

import UIKit

class ViewController3: UIViewController {
    
    private let boxViewTopAndBottomMargin = CGFloat(20)
    private let boxViewLeadgingAndTrailingMargin = CGFloat(20)
    private let labelSpacing = CGFloat(10)
    private let stackView = UIStackView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.greenColor()
        
        setupBoxView()
        setupLabels()
        setupNavigationBarButtonItem()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        setupToolBar()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.setToolbarHidden(true, animated: true)
    }
    
    private func setupNavigationBarButtonItem() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Refresh, target: self, action: #selector(ViewController3.toggleStackViewAxis(_:)))
    }
    
    internal func toggleStackViewAxis(barButtonItem: UIBarButtonItem) {
        UIView.animateWithDuration(0.5) {
            if self.stackView.axis == .Horizontal {
                self.stackView.axis = .Vertical
                self.stackView.alignment = .Fill
            } else {
                self.stackView.axis = .Horizontal
                self.stackView.alignment = .Center
            }
        }
    }
    
    private func setupBoxView() {
        stackView.axis = .Horizontal
        stackView.spacing = labelSpacing
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.distribution = .FillEqually
        stackView.alignment = .Center
        view.addSubview(stackView)
        
        NSLayoutConstraint.activateConstraints([
            stackView.centerYAnchor.constraintEqualToAnchor(view.centerYAnchor, constant: 0),
            stackView.leadingAnchor.constraintEqualToAnchor(view.leadingAnchor, constant: boxViewLeadgingAndTrailingMargin),
            stackView.trailingAnchor.constraintEqualToAnchor(view.trailingAnchor, constant: -boxViewLeadgingAndTrailingMargin)
            ])
    }
    
    private func setupLabels() {
        let label1 = UILabel()
        let label2 = UILabel()
        let label3 = UILabel()
        
        label1.textAlignment = .Center
        label2.textAlignment = .Center
        label3.textAlignment = .Center
        
        label1.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.6)
        label2.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.6)
        label3.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.6)
        
        label1.text = "1"
        label2.text = "2"
        label3.text = "3"
        
        stackView.addArrangedSubview(label1)
        stackView.addArrangedSubview(label2)
        stackView.addArrangedSubview(label3)
    }
    
    private func setupToolBar() {
        
        let barButtonItem1 = UIBarButtonItem(title: "1", style: .Plain, target: self, action: #selector(ViewController3.barButtonItem1Action(_:)))
        let barButtonItem2 = UIBarButtonItem(title: "2", style: .Plain, target: self, action: #selector(ViewController3.barButtonItem2Action(_:)))
        let barButtonItem3 = UIBarButtonItem(title: "3", style: .Plain, target: self, action: #selector(ViewController3.barButtonItem3Action(_:)))
        
        let barButtonFlexibleSpace = UIBarButtonItem(barButtonSystemItem: .FlexibleSpace, target: nil, action: nil)
        navigationController?.setToolbarHidden(false, animated: true)
        navigationController?.toolbar.setItems([
            barButtonFlexibleSpace,
            barButtonItem1,
            barButtonFlexibleSpace,
            barButtonItem2,
            barButtonFlexibleSpace,
            barButtonItem3,
            barButtonFlexibleSpace
            ], animated: true)
    }
    
    internal func barButtonItem1Action(barButtonItem: UIBarButtonItem) {
        toggleLabel(at: 0)
    }
    
    internal func barButtonItem2Action(barButtonItem: UIBarButtonItem) {
        toggleLabel(at: 1)
    }
    
    internal func barButtonItem3Action(barButtonItem: UIBarButtonItem) {
        toggleLabel(at: 2)
    }
    
    private func toggleLabel(at index: NSInteger) {
        stackView.arrangedSubviews[index].hidden = !stackView.arrangedSubviews[index].hidden
    }
}
