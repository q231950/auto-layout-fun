//
//  ViewController4.swift
//  auto-layout-fun
//
//  Created by Martin Kim Dung-Pham on 08.05.16.
//  Copyright © 2016 Martin Kim Dung-Pham. All rights reserved.
//

import UIKit

class ViewController4: UICollectionViewController {
    
    private var items = [CollectionViewItem]()
    private let cellIdentifier = "Cell"
    
    init() {
        let layout = UICollectionViewFlowLayout()
        layout.estimatedItemSize = CGSizeMake(300, 0)
        layout.minimumLineSpacing = 10
        layout.sectionInset = UIEdgeInsetsZero
        
        super.init(collectionViewLayout: layout)
        collectionView?.dataSource = self
        collectionView?.registerClass(XNGCollectionViewCell.self, forCellWithReuseIdentifier: cellIdentifier)
        addItems()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView?.backgroundColor = UIColor.cyanColor()
    }
    
    private func addItems() {
        for _ in 0...1000 {
            items.append(CollectionViewItem())
        }
    }
    
    // MARK: - UICollectionViewDataSource
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1000
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellIdentifier, forIndexPath: indexPath) as! XNGCollectionViewCell
        cell.item = items[indexPath.item]
        
        return cell
    }
    
    
}
